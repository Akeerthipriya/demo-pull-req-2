"""
name: arith.py
purpose: basic arithmetic as function
"""

def add(*args):
    """
    returns the sum of arg1 and arg2
    """

    if len(args) == 0:
        return None

    result = 0
    for a in args:
        __isnum(a)
        result += a

    return result

def subtract(a, b)
    """
    returns the difference between arg1 and arg2
    """
    __isnum(a)
    __isnum(b)
    return a - b

def __isnum(n):
    """
    logic to check arg as number
    raise exception if not number else return True
    """
    return True
